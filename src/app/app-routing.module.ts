import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { languageCodeUrlMatcher } from '@shared/translate';

const routes: Routes = [
  {
    matcher: languageCodeUrlMatcher,
    children: [
      {
        path: 'stuff',
        loadChildren: () => import('@app/stuff').then(m => m.StuffModule),
        title: 'Add Stuff | Stuff and Things Manager',
      },
      {
        path: 'things/add',
        loadChildren: () => import('@app/things/feature-add-things').then(m => m.FeatureAddThingsModule),
        title: 'Add Things | Stuff and Things Manager',
      },
      {
        path: '',
        redirectTo: 'stuff/add',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: 'en',
    pathMatch: 'full',
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
