import {Component, Inject, PLATFORM_ID} from '@angular/core';
import {Meta, Title} from "@angular/platform-browser";
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  view = {
    title: '',
  };
  clientSide: boolean = true;

  constructor(private title: Title) {}

  ngOnInit() {
    this.view.title = this.title.getTitle();
  }
}
