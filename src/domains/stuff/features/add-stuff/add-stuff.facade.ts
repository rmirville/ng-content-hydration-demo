import { Inject, Injectable, InjectionToken, PLATFORM_ID } from "@angular/core";
import { Meta, Title, makeStateKey, MetaDefinition, TransferState } from '@angular/platform-browser';
import { isPlatformServer } from "@angular/common";
import { StuffModule } from "@app/stuff";
import { BehaviorSubject, map, Observable } from "rxjs";

interface AddStuffState {
	title: string;
	meta: MetaDefinition[];
}

const initialState: AddStuffState = {
	title: 'Add Stuff | Stuff And Things Manager',
	meta: [
		{ name: 'author', content: 'Raynald Mirville' },
		{ name: 'title', content: 'Add Stuff | Stuff And Things Manager'},
		{ name: 'description', content: 'Manage your stuff for your convenience.'},
		{ name: 'robots', content: 'index, follow' },
	],
}

@Injectable({
	providedIn: StuffModule,
})
export class AddStuffFacade {
	private store: BehaviorSubject<AddStuffState> = new BehaviorSubject(initialState);
	private state: Observable<AddStuffState> = this.store.asObservable();
	private titleState: Observable<string> = this.state.pipe(map(s => s.title));
	private metaState: Observable<MetaDefinition[]> = this.state.pipe(map(s => s.meta));

	constructor(
		@Inject(PLATFORM_ID)private platformId: Object,
		private transferState: TransferState,
		private metaSvc: Meta,
		private titleSvc: Title,
	) {
		const metaKey = makeStateKey<MetaDefinition[]>('meta');
		if (!this.transferState.hasKey(metaKey)) {
			this.metaState.subscribe(meta => {
				for (const m of meta) {
					if (m.name && this.metaSvc.getTag(m.name)) {
						this.metaSvc.updateTag(m);
					} else {
						this.metaSvc.addTag(m);
					}
				}
				if (isPlatformServer(this.platformId)) {
					this.transferState.set(metaKey, this.store.value.meta);
				}
			});
		}
		this.titleState.subscribe(title => {
			this.titleSvc.setTitle(title);
		});
	}

	public get metaTags(): Observable<MetaDefinition[]> {
		return this.metaState;
	}

	public get title(): Observable<string> {
		return this.titleState;
	}

	public setTitle(title: string): void {
		const oldState: AddStuffState = this.store.value;
		const newState: AddStuffState = { ...oldState, title };
		this.store.next(newState);
	}
	public setMeta(tags: MetaDefinition[]) {
		const oldState: AddStuffState = this.store.value;
		const newState: AddStuffState = { ...oldState, meta: [...tags]};
		this.store.next(newState);
	}
}
