import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddStuffPageComponent } from './pages/add-stuff/add-stuff.page.component';
import { AddStuffComponent } from './components/add-stuff/add-stuff.component';
import { AddStuffRoutingModule } from "./add-stuff-routing.module";
import { SharedModule } from '@shared';

@NgModule({
  declarations: [
    AddStuffPageComponent,
    AddStuffComponent,
  ],
  imports: [
    CommonModule,
    AddStuffRoutingModule,
    SharedModule,
  ]
})
export class AddStuffModule { }
