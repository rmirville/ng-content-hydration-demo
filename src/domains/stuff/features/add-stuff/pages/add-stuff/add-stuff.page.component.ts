import { Component, OnInit } from '@angular/core';
import { AddStuffFacade } from '@app/stuff';

@Component({
  selector: 'app-add-stuff-page',
  templateUrl: './add-stuff.page.component.html',
  styleUrls: ['./add-stuff.page.component.scss']
})
export class AddStuffPageComponent implements OnInit {
  view = {
    title: '',
  };
  clientSide: boolean = true;

  constructor(private facade: AddStuffFacade) { }

  ngOnInit(): void {
    this.facade.title.subscribe(title => {
      this.view.title = title;
    });
  }
}
