import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: 'add',
    title: 'Add Stuff | Stuff and Things Manager',
    loadChildren: () => import('@app/stuff').then(m => m.AddStuffModule),
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
  ]
})
export class StuffRoutingModule { }
