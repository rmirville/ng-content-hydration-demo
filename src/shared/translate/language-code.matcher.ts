import { UrlMatchResult, UrlSegment } from "@angular/router";
import { Dictionary } from "@shared/utility";

export function languageCodeUrlMatcher(url: UrlSegment[]): UrlMatchResult | null {
	const codes: Dictionary<boolean> = {
		'en': true,
		'es': true,
	};
	if (!url.length) return null;
	const code: string = url[0].path;
	const result: UrlMatchResult = {
		consumed: url.slice(0, 1),
		posParams: {
			lang: new UrlSegment(code, { lang: code }),
		},
	};
	return url.length && codes[ code ] ? result : null;
}
